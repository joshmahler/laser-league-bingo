#include "Bingo.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>

using std::cout;
using std::cin;
using std::to_string;

Bingo::Bingo()
{
	/* initialize random seed: */
	srand(time(NULL));

	MAX_MAPS = 26;
	numObjectivesPerMap = 5;
	likelihoodMainObjective = (MAX_LIKELIHOOD - likelihoodWinLose) / choicesMainObjective;
	likelihoodSecondObjective = MAX_LIKELIHOOD / choicesSecondObjective;
	filename = "..\\..\\LaserLeagueBingo.txt";
	Setup();
}

Bingo::~Bingo()
{

}

void Bingo::Setup()
{
	string userInput;
	do
	{
		system("CLS");
		cout << "Welcome to Bingo!\n";
		cout << "1. Total Number of Objectives: " << totalObjectives << "\n";
		cout << "2. Number of Maps: " << MAX_MAPS << "\n";
		cout << "Enter to use defaults, number to change: ";

		userInput = "";
		std::getline(cin, userInput);

		//If user enters 1
		if (userInput == "1")
		{
			//Modify total number of objectives
			totalObjectives += TOTAL_OBJECTIVES_ITERATOR;
			if (totalObjectives > TOTAL_OBJECTIVES_MAX)
			{
				totalObjectives = TOTAL_OBJECTIVES_ITERATOR;
			}
		}
		//If user enters 2
		else if (userInput == "2")
		{
			//Toggle between 26 (old) and 28 (new)
			MAX_MAPS = (MAX_MAPS == 26) ? 28 : 26;
		}
	} while (userInput.length() != 0);

	SetupMainObjectives();
	SetupSecondObjectives();
	SetupMapPowerups();
	SetupClassMods();
	Generate();

	cout << "Generated new bingo options.\n";
}

void Bingo::SetupMainObjectives()
{
	vector<string> temp;
	
	//Win/Lose
	temp = { "Win", "Lose" };
	mainObjs.push_back(temp);

	//Classes
	temp.clear();
	temp = { SMASH, THIEF, SNIPE, SHOCK, BLADE };
	mainObjs.push_back(temp);

	//Collect
	temp.clear();
	temp = { "Collect" };
	mainObjs.push_back(temp);

	//Maps
	temp.clear();
	for (int i = 0; i < MAX_MAPS; ++i)
	{
		temp.push_back(maps[i]);
	}
	mainObjs.push_back(temp);
}

void Bingo::SetupSecondObjectives()
{
	vector<string> temp;

	//Win/Lose
	temp = { "Win", "Lose" };
	secondObjs.push_back(temp);

	//Classes
	temp.clear();
	temp = { SMASH, THIEF, SNIPE, SHOCK, BLADE };
	secondObjs.push_back(temp);

	//Collect
	temp.clear();
	temp = { "Collect" };
	secondObjs.push_back(temp);
}

void Bingo::SetupMapPowerups()
{
	map<string, vector<string>>::iterator it = powerups.begin();

	//Gauntlet
	vector<string> temp = { CHARGE, SHIELD, LOCKDOWN, SPEEDUP, DRAIN };
	powerups.insert(it, std::pair<string, vector<string>>("Gauntlet", temp));

	//Nineclub
	temp.clear();
	temp = { DRAIN, CHARGE, DIVIDE, LOCKDOWN, SPEEDUP };
	powerups.insert(it, std::pair<string, vector<string>>("Nineclub", temp));

	//Spin Cycle
	temp.clear();
	temp = { CHARGE, DIVIDE, DRAIN, SPEEDUP, PAUSE };
	powerups.insert(it, std::pair<string, vector<string>>("Spin Cycle", temp));

	//Arrowhead
	temp.clear();
	temp = { SPEEDUP, SHIELD, DIVIDE, REVIVE, PAUSE };
	powerups.insert(it, std::pair<string, vector<string>>("Arrowhead", temp));

	//Wrapgame
	temp.clear();
	temp = { CHARGE, DRAIN, DIVIDE, SPEEDUP, PAUSE };
	powerups.insert(it, std::pair<string, vector<string>>("Wrapgame", temp));

	//Crusher
	temp.clear();
	temp = { CHARGE, DRAIN, REVIVE, POWER };
	powerups.insert(it, std::pair<string, vector<string>>("Crusher", temp));

	//Warzone
	temp.clear();
	temp = { DIVIDE, POWER, CHARGE, SPEEDUP };
	powerups.insert(it, std::pair<string, vector<string>>("Warzone", temp));

	//Battleground
	temp.clear();
	temp = { SPEEDUP, POWER, SWITCH };
	powerups.insert(it, std::pair<string, vector<string>>("Battleground", temp));

	//Ricochet
	temp.clear();
	temp = { SWITCH, RESET, SPEEDUP, STEAL };
	powerups.insert(it, std::pair<string, vector<string>>("Ricochet", temp));

	//Crossfire
	temp.clear();
	temp = { SWITCH, DRAIN, POWER, CHARGE, SPEEDUP };
	powerups.insert(it, std::pair<string, vector<string>>("Crossfire", temp));

	//Carousel
	temp.clear();
	temp = { SWITCH, DIVIDE, PAUSE, SPEEDUP, STEAL };
	powerups.insert(it, std::pair<string, vector<string>>("Carousel", temp));

	//YControl
	temp.clear();
	temp = { DIVIDE, STEAL, SWITCH, SPEEDUP };
	powerups.insert(it, std::pair<string, vector<string>>("YControl", temp));

	//Prime
	temp.clear();
	temp = { RESET, REVERSE, SPEEDUP, SWITCH, PAUSE };
	powerups.insert(it, std::pair<string, vector<string>>("Prime", temp));

	//Rotator
	temp.clear();
	temp = { DIVIDE, PAUSE, STEAL, SWITCH, SPEEDUP };
	powerups.insert(it, std::pair<string, vector<string>>("Rotator", temp));

	//Turbine
	temp.clear();
	temp = { SWITCH, DIVIDE, REVERSE, SPEEDUP };
	powerups.insert(it, std::pair<string, vector<string>>("Turbine", temp));

	//Chevron
	temp.clear();
	temp = { STUN, LOCKDOWN, SWITCH, DIVIDE, STEAL };
	powerups.insert(it, std::pair<string, vector<string>>("Chevron", temp));

	//Vexation
	temp.clear();
	temp = { LOCKDOWN, STUN, RESET, POWER };
	powerups.insert(it, std::pair<string, vector<string>>("Vexation", temp));

	//Slingshot
	temp.clear();
	temp = { DIVIDE, STUN, RESET, LOCKDOWN, SPEEDUP };
	powerups.insert(it, std::pair<string, vector<string>>("Slingshot", temp));

	//Singularity
	temp.clear();
	temp = { LOCKDOWN, SWITCH, STUN, DIVIDE };
	powerups.insert(it, std::pair<string, vector<string>>("Singularity", temp));

	//Waka
	temp.clear();
	temp = { STUN, DIVIDE, ESCALATE, LOCKDOWN };
	powerups.insert(it, std::pair<string, vector<string>>("Waka", temp));

	//Supernova
	temp.clear();
	temp = { REVERSE, ESCALATE, SPEEDUP };
	powerups.insert(it, std::pair<string, vector<string>>("Supernova", temp));

	//Tube
	temp.clear();
	temp = { ESCALATE, POWER, SWITCH, PAUSE, SPEEDUP };
	powerups.insert(it, std::pair<string, vector<string>>("Tube", temp));

	//Spiral
	temp.clear();
	temp = { STUN, EXPAND, SPEEDUP, LOCKDOWN };
	powerups.insert(it, std::pair<string, vector<string>>("Spiral", temp));

	//Matchsticks
	temp.clear();
	temp = { EXPAND, SPEEDUP, LOCKDOWN };
	powerups.insert(it, std::pair<string, vector<string>>("Matchsticks", temp));

	//Blink
	temp.clear();
	temp = { SWITCH, EXPAND, RESET, LOCKDOWN, STUN };
	powerups.insert(it, std::pair<string, vector<string>>("Blink", temp));

	//Jennifer
	temp.clear();
	temp = { ESCALATE, EXPAND, POWER, STUN };
	powerups.insert(it, std::pair<string, vector<string>>("Jennifer", temp));

	//Fish
	temp.clear();
	temp = { SPEEDUP, STEAL, SWITCH };
	powerups.insert(it, std::pair<string, vector<string>>("Fish", temp));

	//Gong
	temp.clear();
	temp = { CHARGE, LOCKDOWN, POWER, SPEEDUP };
	powerups.insert(it, std::pair<string, vector<string>>("Gong", temp));
}

void Bingo::SetupClassMods()
{
	map<string, vector<string>>::iterator it = classMods.begin();

	//Smash
	vector<string> temp = { ARMOUR, DAZE, COMBO };
	classMods.insert(it, std::pair<string, vector<string>>(SMASH, temp));

	//Ghost
	temp.clear();
	temp = { EXTEND, MEDIC, AGILE };
	classMods.insert(it, std::pair<string, vector<string>>(GHOST, temp));

	//Thief
	temp.clear();
	temp = { EXTEND, SURGE, HEIST };
	classMods.insert(it, std::pair<string, vector<string>>(THIEF, temp));

	//Snipe
	temp.clear();
	temp = { GUARD, REFLEX, NITRO };
	classMods.insert(it, std::pair<string, vector<string>>(SNIPE, temp));

	//Shock
	temp.clear();
	temp = { EXTEND, DAZE, NITRO };
	classMods.insert(it, std::pair<string, vector<string>>(SHOCK, temp));

	//Blade
	temp.clear();
	temp = { HUNTER, REFLEX, RANGE };
	classMods.insert(it, std::pair<string, vector<string>>(BLADE, temp));
}

void Bingo::Generate()
{
	string openSquare = "[\n";
	string beginName = "{\"name\": \"";
	string afterMainObjColon = " ";
	string afterObj = "\"}";
	string comma = ",\n";
	string closeSquare = "\n]";

	string result = "";
	result += openSquare;

	for (int obj = 0; obj < totalObjectives; ++obj)
	{
		result += beginName;

		//Random number [0, MAX_LIKELIHOOD)
		int random = rand() % MAX_LIKELIHOOD;
		
		//For use later
		int mainObj = -1;
		//Start at 1 bc multiply
		for (int i = 1; i <= choicesMainObjective; ++i)
		{
			//Choosing which to make main objective
			if (random < likelihoodWinLose)
			{
				mainObj = 0;
			}
			else if (random < ((i * likelihoodMainObjective) + likelihoodWinLose))
			{
				//i-1 bc vectors start at 0 and this i started at 1
				mainObj = i - 1;
				break;
			}
		}
		if (mainObj == -1)
		{
			//Fixing a logic error (likelihoodMainObjective div in constructor)
			mainObj = choicesMainObjective - 1;
		}

		//Go through the main objs vector to find the main obj
		random = rand() % mainObjs[mainObj].size();
		int savedIndex = random;
		//Use this random to find the choice of main objective
		string word = mainObjs[mainObj][savedIndex];
		result += CustomSwitch(word);
		
		result += afterObj;
		if (obj != totalObjectives - 1)
		{
			result += comma;
		}
	}
	result += closeSquare;

	fstream file;
	file.open(filename, std::ios::out | std::ios::trunc);
	if (file.is_open())
	{
		file.write(result.data(), result.size());
	}
	file.close();
}

string Bingo::CustomSwitch(string obj, string theMap)
{
	string result = "";

	if (obj == "Win" || obj == "Lose")
	{
		result += obj + " ";
		int random = rand() % 6;
		switch (random)
		{
			case 0:
			case 1:
			case 2: //Points
				random = rand() % POINTS_MAX + 1;
				result += to_string(random);
				result += " Point";
				break;
			case 3:
			case 4: //Rounds
				random = rand() % ROUNDS_MAX + 1;
				result += to_string(random);
				result += " Round";
				break;
			case 5: //Games
				random = rand() % GAMES_MAX + 1;
				result += to_string(random);
				result += " Game";
				break;
		}
		if (random != 1)
		{
			result += "s";
		}
	}
	else if (obj == "Smash")
	{
		int random = rand() % 7;
		switch (random)
		{
			case 0:
			case 1:
			case 2: //Single
				random = rand() % SMASH_SINGLE + 1;
				result += to_string(random);
				result += " Single";
				break;
			case 3:
			case 4:
			case 5://Double
				random = rand() % SMASH_DOUBLE + 1;
				result += to_string(random);
				result += " Double";
				break;
			case 6: //Triple
				random = rand() % SMASH_TRIPLE + 1;
				result += to_string(random);
				result += " Triple";
				break;
		}
		result += " " + obj;
		if (random != 1)
		{
			result += "es";
		}
		result += " (" + ClassModifier(obj) + ")";
	}
	else if (obj == "Thief")
	{
		int random = rand() % THIEF_STEAL + 1;
		result += obj + " " + to_string(random);
		result += " Laser";
		if (random != 1)
		{
			result += "s";
		}
		result += " (" + ClassModifier(obj) + ", in one use)";
	}
	else if (obj == "Snipe")
	{
		int random = rand() % 7;

		switch (random)
		{
			case 0: 
			case 1:
			case 2: //Single
				random = rand() % SNIPE_SINGLE + 1;
				result += to_string(random);
				result += " Single";
				break;
			case 3:
			case 4:
			case 5: //Double
				random = rand() % SNIPE_DOUBLE + 1;
				result += to_string(random);
				result += " Double";
				break;
			case 6: //Triple
				random = rand() % SNIPE_TRIPLE + 1;
				result += to_string(random);
				result += " Triple";
				break;
		}
		result += " " + obj;
		if (random != 1)
		{
			result += "s";
		}
		result += " (" + ClassModifier(obj) + ")";
	}
	else if (obj == "Shock")
	{
		int random = rand() % 6;

		switch (random)
		{
			case 0: 
			case 1: 
			case 2: //Single
				random = rand() % SHOCK_SINGLE + 1;
				result += to_string(random);
				result += " Single";
				break;
			case 3:
			case 4: //Double
				random = rand() % SHOCK_DOUBLE + 1;
				result += to_string(random);
				result += " Double";
				break;
			case 5: //Triple
				random = rand() % SHOCK_TRIPLE + 1;
				result += to_string(random);
				result += " Triple";
				break;
		}
		result += " " + obj;
		if (random != 1)
		{
			result += "s";
		}
		result += " (" + ClassModifier(obj) + ")";
	}
	else if (obj == "Blade")
	{
		int random = rand() % 7;

		switch (random)
		{
			case 0:
			case 1:
			case 2:
			case 3: //Single
				random = rand() % BLADE_SINGLE + 1;
				result += to_string(random);
				result += " Single";
				break;
			case 4:
			case 5: //Double
				random = rand() % BLADE_DOUBLE + 1;
				result += to_string(random);
				result += " Double";
				break;
			case 6: //Triple
				random = rand() % BLADE_TRIPLE + 1;
				result += to_string(random);
				result += " Triple";
				break;
		}
		result += " " + obj;
		if (random != 1)
		{
			result += "s";
		}

		result += " (" + ClassModifier(obj) + ")";
	}
	else if (obj == "Collect")
	{
		string key;
		int size;
		if (theMap != "")
		{
			key = theMap;
			size = powerups[theMap].size();
		}
		else 
		{
			//Get a random map
			int random = rand() % MAX_MAPS;
			//Save it as the key
			key = maps[random];
			//Use the key to get the powerups on that map
			size = powerups[key].size();
		}
		//Save the index of a random powerup
		int index = rand() % size;
		//Save a random number of pickups required
		int random = rand() % COLLECT_POWERUPS_MAX + 1;
		//Display
		result += to_string(random);
		result += " " + powerups[key][index] + " Powerup";
		if (random != 1)
		{
			result += "s";
		}
	}
	else //Maps
	{
		//Random number [0, MAX_LIKELIHOOD)
		int random = rand() % MAX_LIKELIHOOD;
		//For use later
		int secondObj = -1;
		//Start at 1 bc multiply
		for (int i = 1; i <= choicesSecondObjective; ++i)
		{
			//Choosing which to make main objective
			if (random < i * likelihoodSecondObjective)
			{
				//i-1 bc vectors start at 0 and this i started at 1
				secondObj = i - 1;
				break;
			}
		}
		//Fixing a logic error (likelihoodSecondObjective div in constructor)
		if (secondObj == -1)
		{
			secondObj = choicesSecondObjective - 1;
		}

		random = rand() % secondObjs[secondObj].size();
		int savedIndex = random;
		//Use this random to find the choice of second objective
		string word = secondObjs[secondObj][savedIndex];
		
		result += CustomSwitch(word, obj);
		result += " (" + obj + ")";
	}

	return result;
}

string Bingo::ClassModifier(string theClass)
{
	int random = rand() % 3;
	return classMods[theClass][random];
}
