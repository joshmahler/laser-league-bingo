#pragma once

#include <string>
#include <fstream>
#include <vector>
#include <map>

using std::fstream;
using std::string;
using std::map;
using std::vector;

class Bingo
{
public:
	Bingo();
	~Bingo();

protected:
	void Setup();
	void SetupMainObjectives();
	void SetupSecondObjectives();
	void SetupMapPowerups();
	void SetupClassMods();
	void Generate();
	string CustomSwitch(string obj, string theMap = "");
	string ClassModifier(string theClass);

	//Hold maps
	//possible powerups should be known for each map
	//all classes/mods can show up on each map

private:
	const int MAX_LIKELIHOOD = 100;
	int MAX_MAPS;
	const int MAX_OBJS_PER_MAP = 10;
	int numObjectivesPerMap;

	const int TOTAL_OBJECTIVES_MAX = 500, TOTAL_OBJECTIVES_ITERATOR = 50;
	int totalObjectives = 200;
	int choicesMainObjective = 4, likelihoodMainObjective, likelihoodWinLose = 10;
	int choicesSecondObjective = 3, likelihoodSecondObjective;
	int POINTS_MAX = 18, ROUNDS_MAX = 6, GAMES_MAX = 3;

	int COLLECT_POWERUPS_MAX = 5;
	
	int THIEF_STEAL = 7, 
		SMASH_SINGLE = 10, SMASH_DOUBLE = 4, SMASH_TRIPLE = 2,
		SNIPE_SINGLE = 10, SNIPE_DOUBLE = 4, SNIPE_TRIPLE = 2,
		SHOCK_SINGLE = 10, SHOCK_DOUBLE = 6, SHOCK_TRIPLE = 3,
		BLADE_SINGLE = 10, BLADE_DOUBLE = 2, BLADE_TRIPLE = 1;

	//Main Objectives
	vector<vector<string>> mainObjs;

	//Second Objectives
	vector<vector<string>> secondObjs;

	//Maps
	const vector<string> maps = { "Gauntlet", "Nineclub", "Spin Cycle",
	"Arrowhead", "Wrapgame", "Crusher", "Warzone", "Battleground", 
	"Ricochet", "Crossfire", "Carousel", "YControl", "Prime",
	"Rotator", "Turbine", "Chevron", "Vexation", "Slingshot",
	"Singularity", "Waka", "Supernova", "Tube", "Spiral",
	"Matchsticks", "Blink", "Jennifer", "Fish", "Gong"};

	//Powerups
	const string REVIVE = "Revive", REVERSE = "Reverse", SWITCH = "Switch",
		DIVIDE = "Divide", SPEEDUP = "Speedup", STEAL = "Steal", 
		RESET = "Reset", POWER = "Power", PAUSE = "Pause", 
		SHIELD = "Shield", DRAIN = "Drain", CHARGE = "Charge", 
		STUN = "Stun", LOCKDOWN = "Lockdown", ESCALATE = "Escalate", 
		EXPAND = "Expand";

	//Classes
	const string SMASH = "Smash", GHOST = "Ghost", THIEF = "Thief",
		SNIPE = "Snipe", SHOCK = "Shock", BLADE = "Blade";

	//Modifiers
	const string ARMOUR = "Armour", DAZE = "Daze", COMBO = "Combo",
		EXTEND = "Extend", MEDIC = "Medic", AGILE = "Agile",
		SURGE = "Surge", HEIST = "Heist", GUARD = "Guard",
		REFLEX = "Reflex", NITRO = "Nitro", HUNTER = "Hunter",
		RANGE = "Range";

	string filename;

	map<string, vector<string>> powerups;
	map<string, vector<string>> classMods;
};

